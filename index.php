<?php

    /*
    
    -- Table: "table"

    -- DROP TABLE "table";

    CREATE TABLE "table"
    (
      alphabetic character varying(255),
      date timestamp without time zone,
      "numeric" numeric,
      unsortable character varying(255)
    )
    WITH (
      OIDS=FALSE
    );

     */
    class Utils {
        public static function arrayget($array, $key, $default = NULL)
        {
            return isset($array[$key]) ? $array[$key] : $default;
        }
    }
    
    class Model {
        private $connection_string = "host=localhost port=5432 dbname=test user=postgres password=postgres";
        private $db;

        private $fields = array(
            "alphabetic" => "string",
            "numeric" => "numeric",
            "date" => "string",
            "unsortable" => "string"
        );

        function __construct()
        {
            $conn_string = "";
            $this->db = pg_connect($this->connection_string);
            $this->generate_seed();
        }

        private function generate_seed()
        {
            $this->exec_query("DELETE from  \"table\"");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('abc',20,'2008-11-24','This')");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('dba',8,'2004-03-01','column')");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('ecd',6,'1979-07-23','cannot')");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('cut',4.2,'1492-12-08','be')");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('001',0,'1601-08-13','sorted.')");
            $this->exec_query("INSERT INTO \"table\" (alphabetic, numeric, date, unsortable) VALUES('eof',2,'1979-07-23','Never.')");
        }

        function exec_query($query)
        {
            return pg_query($this->db, $query);
        }

        function select_query($query)
        {
            $result = array();
            $result_query = $this->exec_query($query);
            while ($row = pg_fetch_array($result_query)){
                array_push($result, $row);
            }
            return $result;
        }

        function get($query, $filters = array(), $orders = array())
        {
            $order_str = " ORDER BY ".join(",",$orders);
            if (count($orders))
            {
                $query .= $order_str;
            }

            $filters = array_map(function($item){
                if ($this->fields[$item[0]] == "numeric") {
                    $item  = $item[0]." = ".(float) $item[1].""; 
                } else {
                    $item  = $item[0]." = '".$item[1]."'"; 
                }
                return $item;
            }, $filters);

            $filter_str = " WHERE ".join(" AND ", $filters);
            if (count($filters))
            {
                $query .= $filter_str;
            }

            return $this->select_query($query);
        }

        function __destruct() {
            pg_close($this->db);
        }
    }

    function is_current_sort($name, $orders) {
        return (in_array($name,$orders));
    }

    $orders = $orders_names = array();
    $filters =  array();
    $sort = Utils::arrayget($_GET,"sort");
    if ($sort)
    {
        $_orders = explode(",", $sort);
        if (count($_orders)) 
        {
            $orders = array_map(function($item) use (&$orders_names){
                $item =  explode("_", $item);
                array_push($orders_names, $item[0]);
                return implode(" ", $item);
            }, $_orders);
        }
    }

    $filter = Utils::arrayget($_GET,"filter");
    if ($filter)
    {
        $_filters = explode(",", $filter);
        if (count($_filters)) 
        {
            $filters = array_map(function($item){
                return explode("=",$item);
            }, $_filters);
        }
    }

    $model = new Model();
    $result = $model->get("SELECT alphabetic, numeric, date, unsortable FROM \"table\"", $filters, $orders);

?>

<!DOCTYPE html>
<html>
<head>
    <title>php sortable table</title>
    <style type="text/css" media="screen">
        .current {
            color:red;
        }
    </style>
</head>
<body>
    <table width="50%">
        <caption>this is a sortable table</caption>
        <thead>
            <tr>
                <th>
                    <a href="?sort=<? if (is_current_sort("alphabetic_ASC",$_orders)): ?>alphabetic_DESC<? else: ?>alphabetic_ASC<? endif; ?>" class="<? if (is_current_sort("alphabetic",$orders_names)): ?>current<? endif; ?>" >Alphabetic</a>
                    <span class="<? if (is_current_sort("alphabetic_ASC",$_orders)): ?>current<? endif; ?>">^</span> 
                    / 
                    <span  class="<? if (is_current_sort("alphabetic_DESC",$_orders)): ?>current<? endif; ?>" >v</span>
                </th>
                <th>
                    <a href="?sort=<? if (is_current_sort("numeric_ASC",$_orders)): ?>numeric_DESC<? else: ?>numeric_ASC<? endif; ?>"  class="<? if (is_current_sort("numeric",$orders_names)): ?>current<? endif; ?>">Numeric </a>
                    <span class="<? if (is_current_sort("numeric_ASC",$_orders)): ?>current<? endif; ?>">^</span> 
                    / 
                    <span class="<? if (is_current_sort("numeric_DESC",$_orders)): ?>current<? endif; ?>">v</span>
                </th>
                <th>
                    <a href="?sort=<? if (is_current_sort("date_ASC",$_orders)): ?>date_DESC<? else: ?>date_ASC<? endif; ?>" class="<? if (is_current_sort("date",$orders_names)): ?>current<? endif; ?>">Date</a> 
                    <span class="<? if (is_current_sort("date_ASC",$_orders)): ?>current<? endif; ?>">^</span> 
                    / 
                    <span class="<? if (is_current_sort("date_DESC",$_orders)): ?>current<? endif; ?>">v</span>
                </th>
                <th>
                    <a href="?sort=<? if (is_current_sort("unsortable_ASC",$_orders)): ?>unsortable_DESC<? else: ?>unsortable_ASC<? endif; ?>"  class="<? if (is_current_sort("unsortable",$orders_names)): ?>current<? endif; ?>">Unsortable</a> 
                    <span class="<? if (is_current_sort("unsortable_ASC",$_orders)): ?>current<? endif; ?>">^</span>
                    /
                    <span class="<? if (is_current_sort("unsortable_DESC",$_orders)): ?>current<? endif; ?>">v</span>
                </th>
            </tr>
        </thead>
        <tbody>
            <? if (isset($result) AND $result ): ?>
                <? foreach ($result as $row): ?>
                    <tr><td><?=$row["alphabetic"]?></td><td><?=$row["numeric"]?></td><td><?=$row["date"]?></td><td><?=$row["unsortable"]?></td></tr>
                <? endforeach; ?>
            <? endif; ?>
        </tbody>
    </table>

    <h2>Multiple sort</h2>
    <p><a href="?sort=date_DESC,alphabetic_DESC" title="">date & alphabetic DESC</a></p>
    <p><a href="?sort=unsortable_ASC,date_ASC" title="">unsortable & date_ASC DESC</a></p>
    <p>etc..</p>

    <h2>Filters</h2>
    <p><a href="?filter=date=1979-07-23" title="">date=1979-07-23</a></p>
    <p><a href="?filter=numeric=20" title="">numeric=20</a></p>
</body>
</html>